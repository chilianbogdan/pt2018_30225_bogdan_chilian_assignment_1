package PT2018.demo.Proiectul1;

import static org.junit.Assert.*;

import org.junit.Test;

public class Tst {

	@Test
	public void testAdnunare() {
		String s1="3x^2+1";
		String s2="2x^1-3";
		Polinom p1=new Polinom(s1);
		Polinom p2=new Polinom(s2);
		Polinom rez = new Polinom("");
		rez = rez.adunarePolinoame(p1, p2);
		String result=rez.t0String();
		assertEquals("3.0x^2+2.0x^1-2.0x^0",result);
	}
	@Test
	public void testScadere() {
		String s1="3x^2+1";
		String s2="2x^1-3";
		Polinom p1=new Polinom(s1);
		Polinom p2=new Polinom(s2);
		Polinom rez = new Polinom("");
		rez = rez.scaderePolinoame(p1, p2);
		String result=rez.t0String();
		assertEquals("3.0x^2-2.0x^1+4.0x^0",result);
	}
	@Test
	public void testInmultire() {
		String s1="3x^2+1";
		String s2="2x^1";
		Polinom p1=new Polinom(s1);
		Polinom p2=new Polinom(s2);
		Polinom rez = new Polinom("");
		rez.inmultire(p1, p2);
		rez.sintetizare(rez);
		String result=rez.t0String();
		assertEquals("6.0x^3+2.0x^1",result);
	}
	@Test
	public void testIntegrare() {
		String s1="3x^2+1";
		Polinom p1=new Polinom(s1);
		p1.integrare();
		String result=p1.t0String();
		assertEquals("1.0x^3+1.0x^1",result);
	}
	@Test
	public void testDerivare() {
		String s1="3x^2";
		Polinom p1=new Polinom(s1);
		p1.derivarePolinom(p1);
		p1.sintetizare(p1);
		String result=p1.t0String();
		assertEquals("6.0x^1",result);
	}

}
