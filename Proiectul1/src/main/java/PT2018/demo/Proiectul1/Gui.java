package PT2018.demo.Proiectul1;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

/**
 * 
 * Clasa de UI care contine atat definitia elementelor grafice cat si definitia
 * Listenerilor si tratarea lor independenta in clase anonime
 * 
 * echivalent View + controller pentru fiecare element grafic
 * 
 *
 */

public class Gui extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JPanel pane = new JPanel(new GridBagLayout());
	GridBagConstraints c = new GridBagConstraints();
	private JButton button1 = new JButton("OK");
	private JButton button2 = new JButton("OK");
	private JButton button3 = new JButton("+");
	private JButton button4 = new JButton("-");
	private JButton button5 = new JButton("*");
	private JButton button6 = new JButton("/");
	private JButton button7 = new JButton("'");
	private JButton button8 = new JButton("S");
	private JTextField text1 = new JTextField(20);
	private JTextField text2 = new JTextField(20);
	private JLabel label1 = new JLabel("P");
	private JLabel label2 = new JLabel("Q");
	// private JLabel label = new JLabel("result");
	String polP;
	String polQ;

	public Gui(String name) {
		super(name);
		c.gridx = 0;
		c.gridy = 0;
		c.gridwidth = 6;
		pane.add(text1, c);

		c.gridx = 0;
		c.gridy = 1;
		c.gridwidth = 6;
		pane.add(text2, c);

		c.gridx = 0;
		c.gridy = 7;
		c.ipady = 10;
		c.gridwidth = 6;
		pane.add(label1, c);

		c.gridx = 0;
		c.gridy = 8;
		c.ipady = 10;
		c.gridwidth = 6;
		pane.add(label2, c);

		c.gridx = 7;
		c.gridy = 0;
		c.ipady = 0;
		pane.add(button1, c);
		button1.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				// TODO Auto-generated method stub
				polP = text1.getText();
				Polinom p = new Polinom(polP);
				p.sintetizare(p);
				label1.setText(p.t0String());
			}

		});
		c.gridx = 7;
		c.gridy = 1;
		pane.add(button2, c);
		button2.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				// TODO Auto-generated method stub
				polQ = text2.getText();

				Polinom q = new Polinom(polQ);
				q.sintetizare(q);
				label2.setText(q.t0String());

			}

		});

		c.gridx = 0;
		c.gridy = 3;
		c.gridwidth = 1;
		pane.add(button3, c);
		button3.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				// TODO Auto-generated method stub
				polP = text1.getText();
				Polinom p = new Polinom(polP);
				p.sintetizare(p);
				polQ = text2.getText();
				Polinom q = new Polinom(polQ);
				q.sintetizare(q);
				Polinom rez = new Polinom("");
				rez = rez.adunarePolinoame(p, q);
				label1.setText(rez.t0String());
				label2.setText(rez.t0String());
			}

		});
		c.gridx = 1;
		c.gridy = 3;
		c.gridwidth = 1;
		pane.add(button4, c);
		button4.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				// TODO Auto-generated method stub
				polP = text1.getText();
				Polinom p = new Polinom(polP);
				p.sintetizare(p);
				polQ = text2.getText();
				Polinom q = new Polinom(polQ);
				q.sintetizare(q);
				Polinom rez = new Polinom("");
				rez = rez.scaderePolinoame(p, q);
				label1.setText(rez.t0String());
				label2.setText(rez.t0String());

			}

		});
		c.gridx = 2;
		c.gridy = 3;
		c.gridwidth = 1;
		pane.add(button5, c);
		button5.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				// TODO Auto-generated method stub
				polP = text1.getText();
				Polinom p = new Polinom(polP);
				p.sintetizare(p);
				polQ = text2.getText();
				Polinom q = new Polinom(polQ);
				q.sintetizare(q);
				Polinom rez = new Polinom("");
				rez.inmultire(p, q);
				rez.sintetizare(rez);
				label1.setText(rez.t0String());
				label2.setText(rez.t0String());
			}

		});
		c.gridx = 3;
		c.gridy = 3;
		c.gridwidth = 1;
		pane.add(button6, c);
		button6.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				// TODO Auto-generated method stub
				polP = text1.getText();
				Polinom p = new Polinom(polP);
				p.sintetizare(p);
				polQ = text2.getText();
				Polinom q = new Polinom(polQ);
				q.sintetizare(q);
				/*
				 * Polinom rez1 = new Polinom(""); Polinom rez2 = new Polinom(""); Polinom
				 * rez[]= { rez1, rez2 }; rez1.impartire label1.setText(rez1.t0String());
				 * label2.setText(rez2.t0String());
				 */
			}

		});
		c.gridx = 4;
		c.gridy = 3;
		pane.add(button7, c);
		button7.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				// TODO Auto-generated method stub
				polP = text1.getText();
				Polinom p = new Polinom(polP);
				p.sintetizare(p);
				polQ = text2.getText();
				Polinom q = new Polinom(polQ);
				q.sintetizare(q);
				p.derivarePolinom(p);
				q.derivarePolinom(q);
				label1.setText(p.t0String());
				label2.setText(q.t0String());

			}

		});
		c.gridx = 5;
		c.gridy = 3;
		pane.add(button8, c);
		button8.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				// TODO Auto-generated method stub
				polP = text1.getText();
				Polinom p = new Polinom(polP);
				p.sintetizare(p);
				polQ = text2.getText();
				Polinom q = new Polinom(polQ);
				q.sintetizare(q);
				p.integrare();
				q.integrare();
				label1.setText(p.t0String());
				label2.setText(q.t0String());
			}

		});

		this.add(pane);

	}

	public static void main(String args[]) {
		JFrame frame = new Gui("GridBagLayoutDemo");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.pack();
		frame.setVisible(true);

	}

}
