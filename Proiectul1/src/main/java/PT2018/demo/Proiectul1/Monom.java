package PT2018.demo.Proiectul1;

public class Monom {

	private double coeficient;
	private int exponent;

	public Monom(double coeficient, int exponent) {
		this.coeficient = coeficient;
		this.exponent = exponent;
	}

	public Monom(Monom monom) {
		// TODO Auto-generated constructor stub
		this.coeficient = monom.getCoeficient();
		this.exponent = monom.getExponent();
	}

	public double getCoeficient() {
		return coeficient;
	}

	public void setCoeficient(double coeficient) {
		this.coeficient = coeficient;
	}

	public int getExponent() {
		return exponent;
	}

	public void setExponent(int exponent) {
		this.exponent = exponent;
	}

}
