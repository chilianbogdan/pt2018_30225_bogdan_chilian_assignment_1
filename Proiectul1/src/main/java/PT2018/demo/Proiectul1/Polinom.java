package PT2018.demo.Proiectul1;

import java.util.ArrayList;

import java.util.List;

public class Polinom {
	String s;
	private List<Monom> lpol = new ArrayList<>(300);

	public Polinom(String s) {
		this.s = s;
		citirePolinom(s);
	}

	public Polinom(List<Monom> temp) {
		// TODO Auto-generated constructor stub
		this.lpol = temp;
	}

	public List<Monom> getPol() {
		return lpol;
	}

	public void setPol(List<Monom> listpol) {
		this.lpol = listpol;
	}

	public void citirePolinom(String pol) {
		lpol = new ArrayList<Monom>();
		String coeff = new String();
		String exxpo = new String();
		String str = pol.replace("-", "+-");
		String[] arr = str.split("\\+");
		for (String p : arr) {
			if ((!p.equals(""))) {
				String[] coef = p.split("x");
				if (coef[0].equals("-") || coef[0].equals("")) {
					coeff = coef[0] + "1";
				} else {
					coeff = coef[0];
				}

				String[] expo = p.split("\\^");
				if (expo.length > 1) {
					exxpo = expo[1];
				} else if (expo.length <= 1) {
					exxpo = "0";

				}

				Monom mon = new Monom(Double.parseDouble(coeff), Integer.parseInt(exxpo));
				lpol.add(mon);
			}
		}

	}

	public String t0String() {
		String s = "";
		for (int i = 0; i < lpol.size(); i++) {

			if ((lpol.get(i).getCoeficient()) > 0 && i > 0) {

				s = s + ("+");
			}
			s = s + (String.valueOf(lpol.get(i).getCoeficient()));
			s = s + ("x^");
			s = s + (String.valueOf(lpol.get(i).getExponent()));

		}
		return s;

	}

	public Polinom adunarePolinoame(Polinom P, Polinom Q) {
		Polinom rez = new Polinom("");
		int ok = 0;
		for (Monom monom : P.getPol()) {
			ok = 0;
			for (Monom monom1 : Q.getPol()) {

				if (monom.getExponent() == (monom1.getExponent())) {
					double c1;
					double c2;
					c1 = monom.getCoeficient();
					c2 = monom1.getCoeficient();
					c1 = c1 + c2;
					ok=1;
					if (c1 != 0) {
						Monom mon = new Monom(c1, monom.getExponent());
						rez.getPol().add(mon);
					}
				}

			}
			if (ok != 1) {
				rez.getPol().add(monom);
			}
		}
		for (Monom monom : Q.getPol()) {
			ok = 0;
			for (Monom monom1 : P.getPol()) {
				if (monom.getExponent() == (monom1.getExponent())) {
					ok = 1;
					break;
				}
			}
			if (ok == 0) {
				rez.getPol().add(monom);
			}
		}
		rez.setPol(sort(rez.getPol()));
		return rez;

	}

	private List<Monom> sort(List<Monom> listpol) {
		int i;
		int j;
		for (i = 0; i < listpol.size() - 1; i++) {
			for (j = i + 1; j < listpol.size(); j++) {
				int m1 = listpol.get(i).getExponent();
				int m2 = listpol.get(j).getExponent();

				if (m1 < m2) {
					Monom m = listpol.get(i);
					listpol.set(i, listpol.get(j));
					listpol.set(j, m);

				}
			}
		}
		return listpol;
	}

	public Polinom scaderePolinoame(Polinom P, Polinom Q) {
		Polinom rez = new Polinom("");
		int ok = 0;
		for (Monom monom : P.getPol()) {
			ok = 0;
			for (Monom monom1 : Q.getPol()) {

				if (monom.getExponent() == (monom1.getExponent())) {
					double c1;
					double c2;
					c1 = monom.getCoeficient();
					c2 = monom1.getCoeficient();
					c1 = c1 - c2;
					if (c1 != 0) {
						Monom mon = new Monom(c1, monom.getExponent());
						rez.getPol().add(mon);
					}
					ok = 1;
				}

			}
			if (ok != 1) {
				rez.getPol().add(monom);
			}
		}
		for (Monom monom : Q.getPol()) {
			ok = 0;
			for (Monom monom1 : P.getPol()) {
				if (monom.getExponent() == (monom1.getExponent())) {
					ok = 1;
					break;
				}
			}
			if (ok == 0) {
				double c1;
				c1 = -monom.getCoeficient();
				monom.setCoeficient(c1);
				rez.getPol().add(monom);
			}
		}

		rez.setPol(sort(rez.getPol()));
		return rez;

	}

	public void derivarePolinom(Polinom P) {

		for (int i = 0; i < P.getPol().size(); i++) {
	
			double c1;
			int p1;
			c1 = P.getPol().get(i).getCoeficient();
			p1 = P.getPol().get(i).getExponent();
			c1 *= p1;
			p1 -= 1;
			if(P.getPol().get(i).getExponent()==0) {
					P.getPol().remove(P.getPol().get(i));
				
			}
			else
			{
			P.getPol().get(i).setCoeficient(c1);
			P.getPol().get(i).setExponent(p1);
			}

		}
	}//

	public void integrare() {

		for (int i = 0; i < lpol.size(); i++) {
			double c1;
			c1 = lpol.get(i).getCoeficient();
			c1 /= lpol.get(i).getExponent() + 1;
			lpol.get(i).setCoeficient(c1);
			int e1;
			e1 = lpol.get(i).getExponent();
			e1++;
			lpol.get(i).setExponent(e1);
		}

	}

	public void inmultire(Polinom p, Polinom q) {

		for (int i = 0; i < p.getPol().size(); i++) {
			for (int j = 0; j < q.getPol().size(); j++) {
				double c1, c2;
				c1 = p.getPol().get(i).getCoeficient();
				c2 = q.getPol().get(j).getCoeficient();
				int e1, e2;
				e1 = p.getPol().get(i).getExponent();
				e2 = q.getPol().get(j).getExponent();
				lpol.add(new Monom(c1 * c2, e1 + e2));
			}
		}
	}

	public void sintetizare(Polinom p) {

		for (int i = 0; i < p.getPol().size(); i++) {
			int j = i + 1;
			for (j = i + 1; j < p.getPol().size(); j++) {
				if (p.getPol().get(i).getExponent() == (p.getPol().get(j).getExponent())) {
					double c1 = p.getPol().get(i).getCoeficient();
					double c2 = p.getPol().get(j).getCoeficient();
					c1 = c1 + c2;
					p.getPol().get(i).setCoeficient(c1);
					p.getPol().remove(p.getPol().get(j));
					j--;
				}
				if(p.getPol().get(i).getExponent()==0 && p.getPol().get(i).getCoeficient()==0) {
					p.getPol().remove(p.getPol().get(j));
				}
			}
		}
		p.sort(p.getPol());

	}

}
